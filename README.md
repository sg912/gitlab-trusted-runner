# gitlab-trusted-runner

Repository for Trusted GitLab Runner. This project is the root project were Trusted Runners are registered. This project also contains the configuration to allow other projects usage of Trusted Runners.

## Request access to Trusted Runners

Add your project to `projects.json` and create a merge request. Please provide the full path of your project (the portion of the URL after "https://gitlab.wikimedia.org/"), and a reason why access is needed. This helps validate if project should be allowed for Trusted Runners.

After a project is added and merged, the `apply-job` has to be started manually by an administrator. A diff is available in the job log of `diff-job`.

### Dockerfile support

Projects can be defined with `"dockerfile": true` to get added to Trusted Runner with Dockerfile support. However this is available for a small group of special-purpose projects only. `dockerfile` defaults to `false` as the majority of images should be build with the secure blubber build frontend.

## Usage of `add-project.py`

The script runs in CI, so for most cases no manual execution is needed. For manual execution make sure to export a valid GitLab API token as `GITLAB_TOKEN`.

Basic usage:

```
usage: add-project.py [-h] [--projects [PROJECTS]] [--instance [INSTANCE]] {diff,apply,verify-config,set-runner-config}

positional arguments:
  {diff,apply,verify-config,set-runner-config}
                        action to perform, either diff, apply, verify-config or set-runner-config

optional arguments:
  -h, --help            show this help message and exit
  --projects [PROJECTS]
                        Filename of configuration file for all projects
  --instance [INSTANCE]
                        hostname of GitLab instance
```

Example to check the diff for a certain configuration:
```
add-project.py diff
```

Output (with color diff):
```
Trusted Runners, running in production:
+project repos/releng/gitlab-runner-test
-project repos/some-old-project

Trusted Dockerfile Runners, running in production:
No changes
```

If diff looks good, run the same command with `apply` instead of `diff`:

```
add-project.py apply
```

## Todos:
 - [ ] add tests for code
 - [x] add requirements.txt
 - [x] add .gitlab-ci.yml / CI for this process
 - [ ] add docs in wikitech/phabricator
 - [ ] add pagination for graphql query (max 100 projects are returned)
