#!/usr/bin/env python3

import argparse
import gitlab
import json
import os
import requests

TRUSTED_REGISTRATION_PROJECT_PATH = "repos/releng/gitlab-trusted-runner"
TRUSTED_TAGS = ["trusted"]
DOCKERFILE_TAGS = ["trusted", "dockerfile"]
TRUSTED_ACCESS_LEVEL = "ref_protected"  # run jobs from protected branches/tags only


# some colors for diffs
class bcolors:
    OKGREEN = "\033[32m"
    FAIL = "\033[31m"
    ENDC = "\033[0m"


def init_gitlab(instance):
    """Initializes gitlab api object

    Parameters:
        instance (str): hostname of GitLab instance
    """
    global gl
    gl = gitlab.Gitlab("https://" + instance, private_token=os.environ["GITLAB_TOKEN"])
    gl.auth()


def get_projects_for_runner(id):
    """Returns all projects for a given Runner. This is not implemented in official gitlab api, so
     graphql api is used. Test querys at https://gitlab.wikimedia.org/-/graphql-explorer.

    Parameters:
        id (str): id of runner

    Raises:
        Exception: Raises exception if GitLab GraphQL API is not responding with http 200

    Returns:
        projects (list(str)): list of project paths
    """
    runner_gid = f"gid://gitlab/Ci::Runner/{id}"
    graphQLQuery = """
{
  runner(id: "%s") {
    id
    projects {
      nodes { id, name, fullPath }
      pageInfo { hasNextPage }
    }
  }
}""" % (
        runner_gid
    )
    gitlabURI = gl.url + "/api/graphql"
    gitlabHeaders = {"Authorization": "Bearer " + os.environ["GITLAB_TOKEN"]}

    response = requests.post(
        gitlabURI, json={"query": graphQLQuery}, headers=gitlabHeaders
    )
    if response.status_code != 200:
        raise Exception(f"Unexpected status code returned: {response.status_code}")

    result = response.json()

    if result["data"]["runner"]["projects"] is None:
        raise SystemExit(
            f"No projects associated with runner {id}.  This is unexpected"
        )

    # Sanity check until improvements are made
    if result["data"]["runner"]["projects"]["pageInfo"]["hasNextPage"]:
        raise Exception(
            "FIXME! get_projects_for_runner needs pagination to get complete results, but it is not implemented yet"
        )

    return [node["fullPath"] for node in result["data"]["runner"]["projects"]["nodes"]]


def get_trusted_runners(tags):
    """Returns all Trusted Runner. Trusted Runners are identified by root registration
     project defined in TRUSTED_REGISTRATION_PROJECT_PATH. Filter for runners by tags.

    Parameters:
        tags: list(str):

     Returns:
         list(gitlab.runner): a list of gitlab.runner objects
    """

    runner_list = []
    # Get all runners from root project
    project = gl.projects.get(TRUSTED_REGISTRATION_PROJECT_PATH)
    for runner in project.runners.list(type="project_type", iterator=True):
        runner_detail = gl.runners.get(runner.id)
        # filter runners by tags
        if tags == runner_detail.tag_list:
            runner_list.append(runner)
    return runner_list


def authorize_project(trusted_projects, command, runner_list):
    """Adds Trusted Runners to a list of trusted projects. Also
     removes access to this projects, if project is removed from list
     of trusted projects.

    Parameters:
        trusted_projects: (list(str)):
        command:
        runner_list: (list(gitlab.runner)): a list of gitlab.runner objects
    """

    # iterate over all Trusted Runners
    for runner in runner_list:
        assigned_projects = get_projects_for_runner(runner.id)
        print("\n" + runner.attributes["description"] + ":")
        changed = False

        # check if assigned projects are still in list of trusted projects (projects.yaml), removes access if not
        for assigned_project in assigned_projects:
            if assigned_project not in trusted_projects:
                changed = True
                try:
                    project = gl.projects.get(assigned_project)
                    print(f"{bcolors.FAIL}-project {assigned_project}{bcolors.ENDC}")
                    if command == "apply":
                        project.runners.delete(runner.id)
                except Exception as e:
                    print(
                        f"{bcolors.FAIL}Failed to delete project {assigned_project} for runner {runner.id}: {e}"
                    )
                    print(
                        f"{bcolors.FAIL}Make sure the runner and project still exist or update projects.json.{bcolors.ENDC}"
                    )

        # check if runner is authorized for all trusted projects (projects.yaml), grants access if not authorized
        for trusted_project in trusted_projects:
            if trusted_project not in assigned_projects:
                changed = True
                try:
                    project = gl.projects.get(trusted_project)
                    print(f"{bcolors.OKGREEN}+project {trusted_project}{bcolors.ENDC}")
                    if command == "apply":
                        project.runners.create({"runner_id": runner.id})
                except Exception as e:
                    print(
                        f"{bcolors.FAIL}Failed to add project {trusted_project} for runner {runner.id}: {e}"
                    )
                    print(
                        f"{bcolors.FAIL}Make sure the runner and project still exist or update projects.json.{bcolors.ENDC}"
                    )

        if not changed:
            print(f"{bcolors.OKGREEN}No changes{bcolors.ENDC}")


def set_runner_config(tags, runner_list):
    """Update config for Trusted Runners to make sure they use the correct
    combination of settings. Settings are tag_list, access_level and locked status.
    Filter for runners by tags (for example trusted or dockerfile).

    Parameters:
       tags: list(str)
       runner_list: (list(gitlab.runner)): a list of gitlab.runner objects
    """
    # iterate over all Trusted Runners
    for runner in runner_list:
        runner_detail = gl.runners.get(runner.id)
        runner_detail.tag_list = tags
        runner_detail.access_level = TRUSTED_ACCESS_LEVEL
        runner_detail.locked = True
        runner_detail.save()
        print("Updated settings for " + runner.attributes["description"])


def load_trusted_projects(projects_filename, dockerfile) -> dict:
    """
    Loads JSON from projects_filename, validates the input, and returns
    it. Optional filter for dockerfile projects if dockerfile is set to true.

    Parameters:
        projects_filename: str: name of projects config file
        dockerfile: bool: only return dockerfile projects

    Returns a dictionary with full project path keys and dictionary
    values (with keys "reason" and possibly "dockerfile").
    """
    trusted_projects = {}

    with open(projects_filename) as f:
        projects = json.load(f)

    # Validate input
    for name, record in projects.items():
        if not isinstance(record, dict) or not record.get("reason"):
            raise Exception(
                f"Invalid record for project {name}: {record}\nA record must be a dictionary with at least at 'reason' key"
            )
        if name.startswith("/"):
            raise Exception(
                f"Invalid project name: {name}\nThe project name must not contain a leading slash"
            )
        # filter for dockerfile projects
        if record.get("dockerfile", False) == dockerfile:
            trusted_projects[name] = record
    # workaround, dockerfile projects have to contain the root project
    if dockerfile:
        trusted_projects[TRUSTED_REGISTRATION_PROJECT_PATH] = projects[
            TRUSTED_REGISTRATION_PROJECT_PATH
        ]

    return trusted_projects


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--projects",
        default="projects.json",
        help="Filename of configuration file for all projects",
        nargs="?",
    )
    parser.add_argument(
        "--instance",
        default="gitlab.wikimedia.org",
        help="hostname of GitLab instance",
        nargs="?",
    )
    parser.add_argument(
        "command",
        help="action to perform, either diff, apply, verify-config or set-runner-config",
        choices=["diff", "apply", "verify-config", "set-runner-config"],
    )
    args = parser.parse_args()

    try:
        # load trusted and dockerfile projects separately
        trusted_projects = load_trusted_projects(args.projects, dockerfile=False)
        dockerfile_projects = load_trusted_projects(args.projects, dockerfile=True)
    except Exception as e:
        raise SystemExit(
            f"{bcolors.FAIL}Failed to load {args.projects} file:\n{e}{bcolors.ENDC}"
        )

    if args.command == "verify-config":
        print(f"{bcolors.OKGREEN}Configuration loaded successfully{bcolors.ENDC}")
        return

    init_gitlab(args.instance)

    # Get a list of all trusted and dockerfile runners
    trusted_runners = get_trusted_runners(TRUSTED_TAGS)
    dockerfile_runners = get_trusted_runners(DOCKERFILE_TAGS)

    if args.command == "set-runner-config":
        set_runner_config(TRUSTED_TAGS, trusted_runners)
        set_runner_config(DOCKERFILE_TAGS, dockerfile_runners)
        return

    # Authorize trusted projects first, then do the same for all dockerfile projects
    authorize_project(trusted_projects.keys(), args.command, trusted_runners)
    authorize_project(dockerfile_projects.keys(), args.command, dockerfile_runners)


if __name__ == "__main__":
    main()
